package vn.com.viettel.offer.bank.constant;

public enum ActionStateENum {

	
	ACTION_NEW(0),ACTION_DEPLOYED(1);
		
		private int value;
		
		private ActionStateENum(int value) {
			this.value = value;
		}
		
		public int value() {
	        return this.value;
	    }

	
}
