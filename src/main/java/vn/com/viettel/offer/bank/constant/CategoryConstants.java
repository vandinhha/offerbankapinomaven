package vn.com.viettel.offer.bank.constant;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Lists;

public final class CategoryConstants {
	private CategoryConstants(){
	}
	public static final HashMap< String, HashMap< Long, String>> TREE_TYPE = new HashMap< String, HashMap< Long, String>>(){
        {
        	//lib tree
            put("lib_tree", new HashMap<Long, String>(){{
            	put(1l, "lib_tree_1l");
            }});
            //par_tree
            put("par_tree", new HashMap<Long, String>(){{
            	put(1l, "par_tree_1l");
            }});
//            int_offer_tree
            put("int_offer_tree", new HashMap<Long, String>(){{
            	put(1l, "int_offer_tree_1l");
            }});
//            int_module_tree
            put("int_module_tree", new HashMap<Long, String>(){{
            	put(1l, "int_module_tree_1l");
            }});
//            int_par_tree
            put("int_par_tree", new HashMap<Long, String>(){{
            	put(1l, "int_par_tree_1l");
            }});
//            int_zone_map_tree
            put("int_zone_map_tree", new HashMap<Long, String>(){{
            	put(1l, "int_zone_map_tree_1l");
            }});
//            int_zone_tree
            put("int_zone_tree", new HashMap<Long, String>(){{
            	put(1l, "int_zone_tree_1l");
            }});
//            int_zone_item_tree
            put("int_zone_item_tree", new HashMap<Long, String>(){{
            	put(1l, "int_zone_item_tree_1l");
            }});
        }
    };

    

}
