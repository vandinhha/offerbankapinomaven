package vn.com.viettel.offer.bank.dto;
// Generated Sep 2, 2016 4:54:08 PM by Hibernate Tools 3.2.1.GA

import java.util.List;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.offer.bank.model.User;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserDTO {

	private long id;

	private String userName;

	private String password;
	
	private String ip;
	
	// thông tin đầu vào  các service
	private List<String> isdns;
	private String offerId;
	private List<String> subids;
	

	

	public List<String> getIsdns() {
		return isdns;
	}

	public void setIsdns(List<String> isdns) {
		this.isdns = isdns;
	}


	public List<String> getSubids() {
		return subids;
	}

	public void setSubids(List<String> subids) {
		this.subids = subids;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public User toBO() {
		User ret = new User();
		ret.setId(this.id);
		ret.setUserName(this.userName);
		ret.setPassword(this.password);
		ret.setIp(this.ip);
		return ret;
	}

	public String getOfferId() {
		return offerId;
	}

	public void setOfferId(String offerId) {
		this.offerId = offerId;
	}



}
