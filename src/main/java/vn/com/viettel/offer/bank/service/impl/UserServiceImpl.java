package vn.com.viettel.offer.bank.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import vn.com.viettel.offer.bank.dto.UserDTO;
import vn.com.viettel.offer.bank.model.User;
import vn.com.viettel.offer.bank.repositories.UserRepository;
import vn.com.viettel.offer.bank.service.UserService;
import vn.com.viettel.offer.bank.utils.DataUtil;

@Service
@Transactional
public class UserServiceImpl implements UserService{

	@Autowired
	private UserRepository userRepository;

	@Override
	public Boolean checkUser(UserDTO userDTO) {
		if(DataUtil.isNullOrEmpty(userDTO.getUserName()) || DataUtil.isNullOrEmpty(userDTO.getPassword()) || DataUtil.isNullOrEmpty(userDTO.getPassword())) {
			return false;
		}
		List<User> check = userRepository.findAllByUserNameAndPasswordAndIp(userDTO.getUserName(), userDTO.getPassword(), userDTO.getIp());
		return check != null && !check.isEmpty();
	}



}
