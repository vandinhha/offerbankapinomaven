package vn.com.viettel.offer.bank.constant;

public final class ErrorConstants {
	
	
	public ErrorConstants() {
		
	}
	public static final String CODE_SUCCESS = "00";
	public static final String CODE_FAILURE = "01";
	public static final String CODE_ERROR_SYSTEMM = "02";
	public static final String REQUEST_ADD = "add";
	public static final String REQUEST_EDIT = "edit";
	public static final String REQUEST_MOVE_TO_CATEGORY = "offerMoveToCategory";
	public static final String REQUEST_DELETE = "delete";
	public static final String REQUEST_CHANGE_STATE = "offerChangeState";
	
	//action
	public static final String REQUEST_Dependencies = "queryDependenciesObj";
	public static final String REQUEST_ACTIONMOVE_TO_CATEGORY = "actionMoveToCategory";
	public static final String REQUEST_ACTIONCHANGE_STATE = "actionChangeState";
	public static final String REQUEST_QueryInfo = "actionIntanceQueryInfo";

}
