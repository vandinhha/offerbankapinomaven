package vn.com.viettel.offer.bank.utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class EncryptFile {

	public static void main(String[] args) {
		List<String> fileNames = java.util.Arrays
				.asList(new String[] { 
						"C:/Users/Ha/Desktop/file/product-catalog-api-0.0.1-SNAPSHOT.part1.rar",
						"C:/Users/Ha/Desktop/file/product-catalog-api-0.0.1-SNAPSHOT.part2.rar",
						"C:/Users/Ha/Desktop/file/product-catalog-api-0.0.1-SNAPSHOT.part3.rar",
						"C:/Users/Ha/Desktop/file/product-catalog-api-0.0.1-SNAPSHOT.part4.rar",
						"C:/Users/Ha/Desktop/file/product-catalog-api-0.0.1-SNAPSHOT.part5.rar",
						"C:/Users/Ha/Desktop/file/product-catalog-api-0.0.1-SNAPSHOT.part6.rar",
						"C:/Users/Ha/Desktop/file/product-catalog-api-0.0.1-SNAPSHOT.part7.rar" });
		for (int i = 0; i < fileNames.size(); i++) {
			try {
				System.out.println("Processing: " + fileNames.get(i));
				RandomAccessFile raf = new RandomAccessFile(fileNames.get(i), "rw");
				byte[] data = new byte[(int) (raf.length())];
				raf.read(data);
				raf.close();
				System.out.println(data.length);
				byte[] dataRemove = new byte[data.length+5]; 
				dataRemove[0] = '1';
				dataRemove[1] = '1';
				dataRemove[2] = '1';
				dataRemove[3] = '1';
				dataRemove[4] = '1';
				for(int j = 0; j < data.length; j++) {
					dataRemove[5+j] = data[j];
				}
				raf.close();
				try {
		            Path path = Paths.get(fileNames.get(i));
		            Files.write(path, dataRemove);
		        } catch (IOException e) {
		            e.printStackTrace();
		        }
				
				System.out.println("OK: " + fileNames.get(i) + "::" + dataRemove.length);
				
			} catch (FileNotFoundException ex) {
				System.out.println("File not found!");
			} catch (IOException ex) {
				ex.printStackTrace();

			}
		}

	}
}
