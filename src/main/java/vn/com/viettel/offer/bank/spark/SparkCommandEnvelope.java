package vn.com.viettel.offer.bank.spark;

public class SparkCommandEnvelope {
		
	private String className = "";
	private String jarFile = ""; 
	private String[] args = null; 

	public SparkCommandEnvelope(){
		
	}
	
	public SparkCommandEnvelope(String className, String jarFile, String[] args){
		this.className = className;
		this.jarFile = jarFile;
		this.args = args;
	}
	
	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getJarFile() {
		return jarFile;
	}

	public void setJarFile(String jarFile) {
		this.jarFile = jarFile;
	}

	public String[] getArgs() {
		return args;
	}

	public void setArgs(String[] args) {
		this.args = args;
	}

}
