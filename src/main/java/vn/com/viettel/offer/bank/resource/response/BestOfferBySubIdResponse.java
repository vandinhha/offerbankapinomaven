package vn.com.viettel.offer.bank.resource.response;

import java.util.List;


public class BestOfferBySubIdResponse {
private List<InfoByOfferId> arrayListInfoByOfferId;
	
	private String listNoOfferId;
	
	public static class InfoByOfferId{
		private String offerId;
		private String listIsdnAndSubId;
		public String getOfferId() {
			return offerId;
		}
		public void setOfferId(String offerId) {
			this.offerId = offerId;
		}
		public String getListIsdnAndSubId() {
			return listIsdnAndSubId;
		}
		public void setListIsdnAndSubId(String listIsdnAndSubId) {
			this.listIsdnAndSubId = listIsdnAndSubId;
		}
		
	}


	public List<InfoByOfferId> getArrayListInfoByOfferId() {
		return arrayListInfoByOfferId;
	}

	public void setArrayListInfoByOfferId(List<InfoByOfferId> arrayListInfoByOfferId) {
		this.arrayListInfoByOfferId = arrayListInfoByOfferId;
	}

	public String getListNoOfferId() {
		return listNoOfferId;
	}

	public void setListNoOfferId(String listNoOfferId) {
		this.listNoOfferId = listNoOfferId;
	}
}
