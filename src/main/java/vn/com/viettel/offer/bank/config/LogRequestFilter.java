package vn.com.viettel.offer.bank.config;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;
import org.springframework.web.util.WebUtils;

import com.google.gson.Gson;

import vn.com.viettel.offer.bank.dto.ServiceLog;

/**
* A filter which logs web requests that lead to an error in the system.
*
*/
@Component
public class LogRequestFilter extends OncePerRequestFilter implements Ordered {

    private final Log logger = LogFactory.getLog(getClass());

    // put filter at the end of all other filters to make sure we are processing after all others
    private int order = Ordered.LOWEST_PRECEDENCE - 8;
    

    @Override
    public int getOrder() {
        return order;
    }

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
        throws ServletException, IOException {
        ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper(request);
        ContentCachingResponseWrapper wrappedRes = new ContentCachingResponseWrapper(response);
        int status = HttpStatus.INTERNAL_SERVER_ERROR.value();

        // pass through filter chain to do the actual request handling
        filterChain.doFilter(wrappedRequest, wrappedRes);
        status = response.getStatus();

       if(status != HttpStatus.INTERNAL_SERVER_ERROR.value() && status != HttpStatus.BAD_REQUEST.value()) {
            try {
            	long startTime = (long)request.getAttribute("pc-startTime");
            	long endTime = System.currentTimeMillis();
            	Object logObj = request.getAttribute("pc-logInfo");
            	if(logObj != null) {
                	ServiceLog serviceLog = (ServiceLog) new Gson().fromJson(String.valueOf(logObj), ServiceLog.class);
                	serviceLog.setRequest(getBody(wrappedRequest));
                	serviceLog.setComment(request.getRequestURI() + ": " + request.getRemoteAddr() + ":  "+ (endTime - startTime) + " ms:  " + serviceLog.getComment());
                	serviceLog.setResponse(getResponsePayload(wrappedRes));
                	logger.info(serviceLog.toString());
            	}
    		} catch (Exception e) {
    			e.printStackTrace(); 
    		}
        }
       //return back ressponse
       wrappedRes.copyBodyToResponse();
        
    }

    private String getBody(ContentCachingRequestWrapper request) {
        // wrap request to make sure we can read the body of the request (otherwise it will be consumed by the actual
        // request handler)
    	String payload = null;
        ContentCachingRequestWrapper wrapper = WebUtils.getNativeRequest(request, ContentCachingRequestWrapper.class);
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                
                try {
                	payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
                }
                catch (UnsupportedEncodingException ex) {
                	payload =  "[unknown]";
                }
            }
        }
        return payload;
    }
    
    
    private String getResponsePayload(HttpServletResponse response) {
        ContentCachingResponseWrapper wrapper = WebUtils.getNativeResponse(response, ContentCachingResponseWrapper.class);
        String payload = null;
        if (wrapper != null) {
            byte[] buf = wrapper.getContentAsByteArray();
            if (buf.length > 0) {
                try {
                    payload = new String(buf, 0, buf.length, wrapper.getCharacterEncoding());
                }
                catch (UnsupportedEncodingException ex) {
                    payload = "[unknown]";
                }

            }
        }
        return payload;
    }


}