package vn.com.viettel.offer.bank.constant;

public enum PriceCompStatesENum {
	 PC_NEW(0),PC_DEPLOYED(1);
	    
	    private int value;
		
		private PriceCompStatesENum(int value) {
			this.value = value;
		}
		
		public int value() {
	        return this.value;
	    }
		
		public static boolean isContain(int value) {
			boolean flag = false;
			for (PriceCompStatesENum _value : values()) {
				if (_value.value == value) {
					flag = true;
					break;
				}
			}
			return flag;
		}
}
