package vn.com.viettel.offer.bank.model;
import static javax.persistence.GenerationType.IDENTITY;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import vn.com.viettel.offer.bank.dto.UserDTO;

@Entity
@Table(name = "PERMIT_USER")
@JsonIgnoreProperties(ignoreUnknown = true)
public class User implements Serializable, Cloneable{

	private static final long serialVersionUID = 274475190788082021L;
	
	
//    @GeneratedValue(strategy=GenerationType.SEQUENCE, generator = "user_sequence")
//    @SequenceGenerator(name = "user_sequence", sequenceName = "PERMIT_USER_SEQ")
	@Id
	@Column(name = "ID", nullable = false)
	@GeneratedValue(strategy=IDENTITY)
	private long id;

	@Column(name = "USER_NAME", nullable = false, unique = true)
	private String userName;

	@JsonIgnore
	@Column(name = "PASS_WORD", nullable = false)
	private String password;
		
	@Column(name = "IP")
	private String ip;


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getUserName() {
		return userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassword() {
		return password;
	}


	public void setPassword(String password) {
		this.password = password;
	}

	public String getIp() {
		return ip;
	}


	public void setIp(String ip) {
		this.ip = ip;
	}


	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	public UserDTO toDTO() {
		UserDTO ret  = new UserDTO();
		ret.setId(this.id);
		ret.setUserName(this.userName);
		ret.setPassword(this.password);
		ret.setIp(this.ip);
		return ret;
	}
	
	
}
