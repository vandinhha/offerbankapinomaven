package vn.com.viettel.offer.bank.constant;

public enum OfferStateENum {
	
	OFFER_NEW(0),OFFER_DEPLOYED(1);
	
	private int value;
	
	private OfferStateENum(int value) {
		this.value = value;
	}
	
	public int value() {
        return this.value;
    }

}
