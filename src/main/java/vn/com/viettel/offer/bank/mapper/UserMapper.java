package vn.com.viettel.offer.bank.mapper;

import java.util.List;

import com.google.common.collect.Lists;

import vn.com.viettel.offer.bank.dto.UserDTO;
import vn.com.viettel.offer.bank.model.User;
import vn.com.viettel.offer.bank.utils.DataUtil;

public class UserMapper {
	public static User toBO(UserDTO userDTO) {
		if(userDTO == null) {
			return null;
		}
		User ret  = new User();
		ret.setId(userDTO.getId());
		ret.setUserName(userDTO.getUserName());
		ret.setPassword(userDTO.getPassword());
		ret.setIp(userDTO.getIp());
		return ret;
	}
	
	public static UserDTO toDTO(User obj) {
		if(obj == null) {
			return null;
		}
		UserDTO ret  = new UserDTO();
		ret.setId(obj.getId());
		ret.setUserName(obj.getUserName());
		ret.setPassword(obj.getPassword());
		ret.setIp(obj.getIp());
		return ret;
	}
	
	
	public static List<UserDTO> toListDTO(List<User> obj) {
		if(DataUtil.isListNullOrEmpty(obj)) {
			return Lists.newArrayList();
		}
		List<UserDTO> ret = Lists.newArrayList();
		for (User item : obj) {
			ret.add(toDTO(item));
		}
		return ret;
	}
	
	
	public static List<User> toListBO(List<UserDTO> obj) {
		if(DataUtil.isListNullOrEmpty(obj)) {
			return Lists.newArrayList();
		}
		List<User> ret = Lists.newArrayList();
		for (UserDTO item : obj) {
			ret.add(toBO(item));
		}
		return ret;
	}
	
	
}
