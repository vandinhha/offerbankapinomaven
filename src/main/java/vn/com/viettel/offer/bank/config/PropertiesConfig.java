package vn.com.viettel.offer.bank.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties
public class PropertiesConfig {

	@Value("${api.authen.userName}")
	private String appUserName;

	@Value("${api.authen.password}")
	private String appPassword;

	@Value("${spring.spark.server.className}")
	private String className;

	@Value("${spring.spark.server.pathJarFile}")
	private String jarFile;


	@Value("${spring.spark.server.host}")
	private String host;

	@Value("${spring.spark.server.port}")
	private String port;

	@Value("${spring.spark.server.timeOut}")
	private String timeOut;



	
	@Value("${redis.define.connect.string}")
	private String redisDefine;

	@Value("${redis.define.connect.name}")
	private String redisName;

	@Value("${redis.define.database.index}")
	private int dbRedisIndex;

	public String getAppUserName() {
		return appUserName;
	}

	public void setAppUserName(String appUserName) {
		this.appUserName = appUserName;
	}

	public String getAppPassword() {
		return appPassword;
	}

	public void setAppPassword(String appPassword) {
		this.appPassword = appPassword;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getJarFile() {
		return jarFile;
	}

	public void setJarFile(String jarFile) {
		this.jarFile = jarFile;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	public String getRedisDefine() {
		return redisDefine;
	}

	public void setRedisDefine(String redisDefine) {
		this.redisDefine = redisDefine;
	}

	public String getRedisName() {
		return redisName;
	}

	public void setRedisName(String redisName) {
		this.redisName = redisName;
	}

	public int getDbRedisIndex() {
		return dbRedisIndex;
	}

	public void setDbRedisIndex(int dbRedisIndex) {
		this.dbRedisIndex = dbRedisIndex;
	}

}
