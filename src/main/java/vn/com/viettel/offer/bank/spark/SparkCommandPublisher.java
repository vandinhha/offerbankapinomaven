package vn.com.viettel.offer.bank.spark;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;


public class SparkCommandPublisher {
	
	private String dstHost;
	private int dstPort;
	private int timeOut;
	
	SparkCommandPublisher(){

	}
	
	public SparkCommandPublisher(String dstHost, int dstPort, int timeout){
		this.dstHost = dstHost;
		this.dstPort = dstPort;
		this.timeOut = timeout;
	}	
	
	public String publish(SparkCommandEnvelope cmdEnve) throws IOException {
		String responseStr = "";
		if(cmdEnve != null) {
			Socket server = null;
			ObjectOutputStream writer = null;
			BufferedReader reader = null;
			try {
				InetSocketAddress isa = new InetSocketAddress(dstHost, dstPort);
				server = new Socket();			
				server.connect(isa, timeOut);
				writer = new ObjectOutputStream(server.getOutputStream());
				reader = new BufferedReader(new InputStreamReader(server.getInputStream()));
				writer.writeObject(cmdEnve);
				writer.flush();
				responseStr = reader.readLine();
				
			} catch (Exception e) {
				System.out.println("Error in SparkCommandPublisher: ");
		        e.printStackTrace();
		        return "sparkError";
			} finally {
				if(server != null) {server.close();};
				if(writer != null) {writer.close();};
				if(reader != null) {reader.close();};
			}
			
		} else {
			responseStr = "SparkClientSender says cmd enve has not been set yet";
		}
		
		return responseStr;
	}	  

}
